jQuery(function($){
	$('.loadmore').click(function(){
 
		var button = $(this), data = {
			'action' : 'loadmore',
			'max' : max_page_myajax,
			'page' : current_page_myajax,
			'query' : posts_myajax // that's how we get params from wp_localize_script() function
		};

		console.log(data);

		$.ajax({
			url	: load_more_params.ajax_url, // AJAX handler
			//url : '/wp-admin/admin-ajax.php',
			type : 'POST',
			data : data,
			beforeSend : function ( xhr ) 
			{
				console.log("Loading");
		
				button.text('Indlæser...'); // change the button text, you can also add a preloader image
				//button.html('<img src="/loading.gif">');
			},
			success : function( data )
			{
				console.log("Success");

				if(data) 
				{ 
					console.log("We got data!");
					console.log("Current page = " + current_page_myajax);
					console.log("Number of pages = " + max_page_myajax);

					$('#articles').append(data);

					button.text( 'Flere indlæg' ).prev().before(data); // insert new posts
					current_page_myajax++;
 
					if ( current_page_myajax == max_page_myajax ) 
					{
						console.log("Last page! Remove button");

						button.remove(); // if last page, remove the button
					}
 
				} else {
					
					console.log("No data! Remove button");

					button.remove(); // if no data, remove the button as well
				}
			},
			error : function() 
			{
	            console.log("jQuery error!");   
      
	        }
		});
	});	
});