<?php

/**
		/*
	     * Template Name: Arkiv
	     * Description: A Page Template with custom post list
	     */

/**

 * The template for displaying archive pages.
 *
 * Learn more: https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package storefront
 */

	get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div class="woocommerce columns-3">
				<ul id="articles" class="products columns-3">

					<?php 

					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

			        $query_args = array(
			        	'posts_per_page' 	=> 9, // Value "-1" displays all products in feed	
			            'post_type' 		=> 'post',
			            'post_status' 		=> 'publish',
					    'orderby' 			=> 'publish_date',
					    'order' 			=> 'DESC',
			            'paged'				=> $paged
			        );

			        $wp_query = new WP_Query( $query_args); 

			        if( $wp_query->have_posts() ) : while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

							<li class="product post-item post-list-item">
								
								<div class="post-image-container">
									<a class="post-image" href="<?php the_permalink(); ?>">
										<?php 
											if ( has_post_thumbnail()) 
											{
												the_post_thumbnail();
											}
										?>
									</a>							
								</div>
								
								<h2 class="post-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
								
								<span class="post-category"><?php the_category(', ');?></span>
							</li>

					<?php endwhile; ?> <?php wp_reset_postdata(); ?> <?php endif; ?>

				</ul>

				<nav>
					<?php if (  $wp_query->max_num_pages > 1 )
					{
						echo '
							<div class="wordpress_wrapper">
								<div class="loadmore" id="wordpress_loadmore">Flere indlæg</div>
							</div>'; 
					} ?>
				</nav>

				<script type='text/javascript'>
					/* <![CDATA[ */
					var posts_myajax = '<?php echo json_encode( $wp_query->query_vars ) ?>',
				    current_page_myajax = 1,
				    max_page_myajax = <?php echo $wp_query->max_num_pages ?>
				    /* ]]> */
				</script>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
/*do_action( 'storefront_sidebar' );*/
get_footer();
