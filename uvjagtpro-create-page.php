<?php

	/**
	* Plugin Name: UVjagtPro - Load more post list 
	* Description: This plugin creates a page when plugin is activated.
	* Author: Kim Nyegaard Andreasen
	* Version: 1.0
	*/

	include "pagetemplater.php";

	/*######################################################################################################
	########################################################################################################
	########################################################################################################
	########################################################################################################
	############################### Parsing variables to script ############################################
	########################################################################################################
	########################################################################################################
	########################################################################################################
	########################################################################################################*/

	add_action( 'wp_enqueue_scripts', 'my_load_more' );

	function my_load_more() 
	{
		// register our main script but do not enqueue it yet
		wp_register_script( 'load_more', plugins_url( 'myloadmore.js', __FILE__ ), array('jquery') );

		$postArray = array(
			//'posts' 	=> $wp_query->query_vars, // everything about your loop is here
			'ajax_url'	=> admin_url( 'admin-ajax.php' ) // WordPress AJAX
		);

		wp_localize_script( 'load_more', 'load_more_params', $postArray);
	 
	 	wp_enqueue_script( 'load_more' );

	}

	/*######################################################################################################
	########################################################################################################
	########################################################################################################
	########################################################################################################
	############################### AJAX handler ###########################################################
	########################################################################################################
	########################################################################################################
	########################################################################################################
	########################################################################################################*/

	add_action( 'wp_ajax_loadmore', 'load_more_ajax' ); // wp_ajax_{action}
	add_action( 'wp_ajax_nopriv_loadmore', 'load_more_ajax' ); // wp_ajax_nopriv_{action}

	function load_more_ajax() 
	{
	    
		// prepare our arguments for the query
	    $query_vars = json_decode( stripslashes( $_POST['query'] ), true );
	    $query_vars['paged'] = $_POST['page'] + 1;
	    $query_vars['post_status'] = 'publish';

	    $posts = new WP_Query( $query_vars );

	    if ($posts->have_posts()) : while ($posts->have_posts()) : $posts->the_post(); ?>

			<li class="product post-item">
										
				<div class="post-image-container">
					<a class="post-image" href="<?php the_permalink(); ?>">
						<?php 
							if ( has_post_thumbnail()) 
							{
								the_post_thumbnail();
							}
						?>
					</a>							
				</div>
				
				<h2 class="post-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
				
				<span class="post-category"><?php the_category(', ');?></span>
			</li>	

    	<?php endwhile; endif;

	    wp_die();
	}

?>